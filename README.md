# bashnlu

`bashnlu` is a tool/toy for performing intent classification / dialogue management in Bash. Nothing is used except your typical shell tools, such as `grep`, `sed`, `awk` etc.

There is probably a lot that can be improved. If you have suggestions, get in touch at `jonne (at) jonnesaleva.com`.

## How to use

The cool thing about `bashnlu` is that it reads and writes using `stdin/stdout`.

Before getting started, be sure to set the `BASHNLU_RULES` environment variable to point to the rules file.

To classify an intent, simply pipe in some text to `bashnlu infer`

```
> echo "how much disk space do i have" | ./bashnlu infer
< disk_space
```

You can then pipe this intent to `bashnlu execute`

```
> echo "disk_space" | ./bashnlu execute
< Your disk space: 19G
```

You can also use things like `rofi`, `dmenu` etc. to handle the input/output.

## How does it work?

Underneath the hood `bashnlu` matches the input string against a cascade of regular expressions.

Each matched regular expression emits a parsed intent to `stdout`, and the most frequent intent is chosen as the intent `bashnlu` uses.

More intents can be added by creating a larger file like `sample.rules`.

## Future ideas

- [] Decoupling NLU and Dialogue Manager
- [] Slot filling & confirmation turns
- [] Handle action execution by allowing the user to simply point to scripts.

## License information

```
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```
